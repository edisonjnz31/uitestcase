//
//  UITESTUITests.swift
//  UITESTUITests
//
//  Created by Edison on 4/10/17.
//  Copyright © 2017 Edison. All rights reserved.
//

import XCTest

class UITESTUITests: XCTestCase {
    
    let app = XCUIApplication()
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        app.launchArguments = ["UITests"]
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()
        UIApplication.shared.keyWindow?.layer.speed = 100
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testOverall() {
        app.buttons["register"].tap()
        testRegister()
        app.navigationBars.buttons.element(boundBy: 0).tap()
        testLogin()
        
        let  coordinate2:XCUICoordinate = app.coordinate(withNormalizedOffset: CGVector(dx: 0, dy: 200))
        let coordinate1 = coordinate2.withOffset(CGVector(dx:100,dy:200))
        coordinate2.press(forDuration: 1, thenDragTo: coordinate1)
        sleep(2)
        app.tables["Table"].cells["Menu 1"].tap()
        testCollectionExample()
        app.navigationBars.buttons.element(boundBy: 0).tap()
        app.navigationBars.buttons.element(boundBy: 0).tap()
        app.tables["Table"].cells["Menu 2"].tap()
        testViewPager()
    
    }
    
    func testExample() {
        app.buttons["register"].tap()
        
        print("WOW", app.debugDescription)
        app.swipeRight()
        let view = app.otherElements["UIView"]
        let  coordinate2:XCUICoordinate = app.coordinate(withNormalizedOffset: CGVector(dx: 0, dy: 200))
        let coordinate1 = coordinate2.withOffset(CGVector(dx:100,dy:200))
        coordinate2.press(forDuration: 1, thenDragTo: coordinate1)
        sleep(2)
        
    }
    
    func testCollectionExample() {
        let customCollection = app.collectionViews["Custom Collection"]
        customCollection.swipeUp()
        customCollection.swipeDown()
        customCollection.cells["ITEM NO. 5"].tap()
        app.navigationBars.buttons.element(boundBy: 0).tap()
        customCollection.swipeUp()
        customCollection.cells["ITEM NO. 18"].tap()
        app.navigationBars.buttons.element(boundBy: 0).tap()
    }
    
    func testLogin() {
        let app = XCUIApplication()
        print("WOW", app.debugDescription)
        app.buttons["loginButton"].tap()
        if app.alerts["email error"].exists {
            print("ENEMY EXISTS")
            app.buttons["OK"].tap()
        } else {
            print("email error fails")
            app.terminate()
        }
        
        
        app.textFields["loginEmail"].tap()
        app.textFields["loginEmail"].typeText("test")
        app.scrollViews["loginScroll"].swipeDown()
        app.buttons["loginButton"].tap()
        
        
        
        if app.alerts["password error"].exists {
            print("ENEMY EXISTS")
            app.buttons["OK"].tap()
        } else {
            print("password error fails")
            app.terminate()
        }
        
        app.secureTextFields["loginPassword"].tap()
        app.secureTextFields["loginPassword"].typeText("test")
        app.scrollViews["loginScroll"].swipeDown()
        app.buttons["loginButton"].tap()
        
        if app.alerts["wrong email"].exists {
            print("REGEX ENEMY")
            app.buttons["OK"].tap()
        } else {
            print("wrong email fails")
            app.terminate()
        }
        
        app.textFields["loginEmail"].press(forDuration: 1.0)
        app.menuItems["Select All"].tap()
        app.textFields["loginEmail"].typeText("test@test.com")
        
        app.scrollViews["loginScroll"].swipeDown()
        app.buttons["loginButton"].tap()
        
        if app.alerts["error login"].exists {
            print("ENEMY ERROR LOGIN")
            app.buttons["OK"].tap()
        } else {
            print("error login fails")
            app.terminate()
        }
        
        app.secureTextFields["loginPassword"].press(forDuration: 1.0)
        app.menuItems["Select All"].tap()
        app.secureTextFields["loginPassword"].typeText("test123")
        app.scrollViews["loginScroll"].swipeDown()
        app.buttons["loginButton"].tap()
//        XCTAssert(app.navigationBars["Dashboard"].exists)
    }
    
    func testRegister() {
        let app = XCUIApplication()
        
        app.buttons["registerButon"].tap()
        app.textFields["firstName"].tap()
        app.textFields["firstName"].typeText("ferry")
        app.buttons["registerButon"].tap()
        app.buttons["OK"].tap()
        
        app.textFields["lastName"].tap()
        app.textFields["lastName"].typeText("irawan")
        app.buttons["registerButon"].tap()
        app.buttons["OK"].tap()
        
        app.textFields["email"].tap()
        app.textFields["email"].typeText("ferry@gmail.com")
        app.buttons["registerButon"].tap()
        app.buttons["OK"].tap()
        
        app.textFields["password"].tap()
        app.textFields["password"].typeText("qwerty")
        app.buttons["registerButon"].tap()
        app.buttons["OK"].tap()
        
    }
    
    func testViewPager() {
        app.otherElements["topContainer"].swipeLeft()
        app.otherElements["bottomContainer"].swipeRight()
        app.otherElements["topContainer"].buttons["showMain"].tap()
    }
    
}


