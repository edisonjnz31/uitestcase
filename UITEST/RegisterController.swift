//
//  RegisterController.swift
//  UITEST
//
//  Created by Ferry Irawan on 11/4/17.
//  Copyright © 2017 Edison. All rights reserved.
//

import UIKit

class RegisterController: UIViewController {
    
    @IBOutlet weak var firstNameText: UITextField!
    @IBOutlet weak var lastNameText: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onClickRegister(_ sender: AnyObject) {
        
        guard firstNameText.text != "" else {
            showErrorAlertWithMessage(title: "first name error", message: "First Name field cannot be empty")
            return
        }
        
        guard lastNameText.text != "" else {
            showErrorAlertWithMessage(title: "last name error", message: "Last Name field cannot be empty")
            return
        }
        
        guard txtEmail.text != "" else {
            showErrorAlertWithMessage(title: "email error", message: "email field cannot be empty")
            return
        }
        
        guard txtPassword.text != "" else {
            showErrorAlertWithMessage(title: "password error", message: "password field cannot be empty")
            return
        }
        
        guard emailRegexCheck(email: txtEmail.text!) else {
            showErrorAlertWithMessage(title: "wrong email", message: "email not email format")
            return
        }
        
        showErrorAlertWithMessage(title: "login success", message: "login success")
    }
    
    func emailRegexCheck(email: String) -> Bool{
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,20}"
        if ( NSPredicate(format: "SELF MATCHES %@", emailRegEx).evaluate(with: email.trimmingCharacters(in: NSCharacterSet.whitespaces))) {
            return true
        }
        else {
            return false
        }
    }
    
}
