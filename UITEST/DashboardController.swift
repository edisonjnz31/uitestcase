//
//  DashboardController.swift
//  UITEST
//
//  Created by Edy Sudarto on 4/12/17.
//  Copyright © 2017 Edison. All rights reserved.
//

import Foundation
import UIKit

class DashboardController: UIViewController {
    
    @IBOutlet weak var textLabel: UILabel!
    var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textLabel.text = "Menu Option is currently unavailable !"
    }
}
