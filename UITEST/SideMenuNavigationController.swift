//
//  SideMenuNavigationController.swift
//  UITEST
//
//  Created by Edy Sudarto on 4/11/17.
//  Copyright © 2017 Edison. All rights reserved.
//

import UIKit
import SideMenuController

class SideMenuNavigationController: UIViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}


class DrawerInitiateViewController: SideMenuController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        performSegue(withIdentifier: "embedSideController", sender: nil)
        performSegue(withIdentifier: "containSideMenu", sender: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let menuIcon = UIImage(named:"menu-icon")
        let resizeIcon = self.imageWithImage(image: menuIcon!, scaledToSize: CGSize(width: 24, height: 14))
        SideMenuController.preferences.interaction.menuButtonAccessibilityIdentifier = "drawer"
        SideMenuController.preferences.drawing.menuButtonImage = resizeIcon
        SideMenuController.preferences.drawing.sidePanelPosition = .underCenterPanelLeft
        SideMenuController.preferences.drawing.sidePanelWidth = 220
        SideMenuController.preferences.drawing.centerPanelShadow = true
        SideMenuController.preferences.animating.statusBarBehaviour = .fadeAnimation
        SideMenuController.preferences.interaction.swipingEnabled = true
    }
    
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
}

class TestCaseTableCell: UITableViewCell {
    
    @IBOutlet weak var customLabel: UILabel!

}


class TestTableViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var customTableView: UITableView!
    
    var menus = ["Collection View", "View Pager"]
    var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customTableView.accessibilityLabel = "Table"
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count + 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! TestCaseTableCell
        
        cell.customLabel.text = "Menu Option \(indexPath.item + 1)"
        cell.accessibilityLabel = "Menu \(indexPath.item + 1)"
        
        if indexPath.item < 2 {
            cell.customLabel.text = menus[indexPath.item]
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.item < 2 {
            self.sideMenuController?.centerViewController.childViewControllers[0].performSegue(withIdentifier: menus[indexPath.item], sender: nil)
            
        }
        else {
            selectedIndex = indexPath.item + 1
            self.sideMenuController?.centerViewController.childViewControllers[0].performSegue(withIdentifier: "showViewDetail", sender: nil)
            
        }
        self.sideMenuController?.toggle()
    }
    
}

