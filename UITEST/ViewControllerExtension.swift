//
//  ViewControllerExtension.swift
//  UITEST
//
//  Created by harsanto on 4/11/17.
//  Copyright © 2017 Edison. All rights reserved.
//

import UIKit

extension UIViewController {
    func showErrorAlertWithMessage(title: String, message: String) {
        let alertController = UIAlertController(title: title, message:message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
}
