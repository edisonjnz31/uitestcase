//
//  LoginController.swift
//  UITEST
//
//  Created by harsanto on 4/12/17.
//  Copyright © 2017 Edison. All rights reserved.
//

import UIKit

class LoginController: UIViewController {

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        self.view.accessibilityLabel = "tapView"
        // Do any additional setup after loading the view.
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onClickLogin(_ sender: AnyObject) {
        guard txtEmail.text != "" else {
            showErrorAlertWithMessage(title: "email error", message: "email field cannot be empty")
            return
        }
        
        guard txtPassword.text != "" else {
            showErrorAlertWithMessage(title: "password error", message: "password field cannot be empty")
            return
        }
        
        guard emailRegexCheck(email: txtEmail.text!) else {
            showErrorAlertWithMessage(title: "wrong email", message: "email not email format")
            return
        }
        
        guard txtEmail.text == "test@test.com" && txtPassword.text == "test123" else {
            showErrorAlertWithMessage(title: "error login", message: "email or password is wrong")
            return
        }
        
        performSegue(withIdentifier: "showMainView", sender: nil)
//        showErrorAlertWithMessage(title: "login success", message: "login success")
    }
    
    func emailRegexCheck(email: String) -> Bool{
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,20}"
        if ( NSPredicate(format: "SELF MATCHES %@", emailRegEx).evaluate(with: email.trimmingCharacters(in: NSCharacterSet.whitespaces))) {
            return true
        }
        else {
            return false
        }
    } 

}
