//
//  CollectionViewTestController.swift
//  UITEST
//
//  Created by Edy Sudarto on 4/11/17.
//  Copyright © 2017 Edison. All rights reserved.
//

import Foundation
import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var textLabel: UILabel!
    
    
}

class CollectionViewTestController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var customCollectionView: UICollectionView!
    
    var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Collection View"
        customCollectionView.delegate = self
        customCollectionView.dataSource = self
        customCollectionView.collectionViewLayout = CustomCollectionFlowLayout()
        customCollectionView.accessibilityLabel = "Custom Collection"
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 21
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CustomCollectionViewCell
        
        cell.textLabel.text = "ITEM NO. \(indexPath.item + 1)"
        cell.accessibilityLabel = "ITEM NO. \(indexPath.item + 1)"
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.green.cgColor
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.item + 1
        performSegue(withIdentifier: "showDetail", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            let nextVC = segue.destination as! CollectionViewDetailController
            nextVC.selectedIndex = selectedIndex
        }
    }
    
    
}

class CollectionViewDetailController: UIViewController {
    
    @IBOutlet weak var textLabel: UILabel!
    var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textLabel.text = "WOW !\nYOU HAVE SELECTED ITEM NO. \(selectedIndex)"
    }
}

class CustomCollectionFlowLayout: UICollectionViewFlowLayout {
    
    override init() {
        super.init()
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }
    
    override var itemSize: CGSize {
        set {
            
        }
        get {
            let numberOfColumns: CGFloat = 3
            let itemWidth = (self.collectionView!.frame.width - (numberOfColumns - 1)) / numberOfColumns
            return CGSize(width: itemWidth,height: 150)
        }
    }
    
    func setupLayout() {
        minimumInteritemSpacing = 0
        minimumLineSpacing = 0
        scrollDirection = .vertical
    }
    
    
}
